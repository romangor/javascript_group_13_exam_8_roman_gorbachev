export class QuoteService {
  categories: string[] = ['star-wars', 'famous-people', 'saying', 'humour', 'motivational'];

  getCategories(){
    return this.categories.slice();
  }

}
