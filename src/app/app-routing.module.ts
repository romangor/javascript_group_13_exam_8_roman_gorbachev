import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { QuotesComponent } from './quotes/quotes.component';
import { AddQuoteComponent } from './add-quote/add-quote.component';

const routes: Routes = [{path: 'quotes', component: HomeComponent, children: [
                            {path: '', component: QuotesComponent},
                            {path: ':id', component: QuotesComponent},
                              {path: ':id/:id/edit', component: AddQuoteComponent},
                              {path: ':id/edit', component: AddQuoteComponent}
                            ]},
                        {path: 'addQuote', component: AddQuoteComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
