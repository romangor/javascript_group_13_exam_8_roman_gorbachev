import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { QuoteService } from '../shared/quote.service';
import { ActivatedRoute, Params } from '@angular/router';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-add-quote',
  templateUrl: './add-quote.component.html',
  styleUrls: ['./add-quote.component.css']
})
export class AddQuoteComponent implements OnInit {
  categories!: string[];

  constructor(private quoteService: QuoteService,
              private http: HttpClient,
              private route: ActivatedRoute) { }
  author!: string;
  category!: string;
  text!: string;
  id!: string;
  ngOnInit(): void {
    const categories = this.quoteService.getCategories();
    this.categories = categories.map(category => {
      if (!category) return category;
      return category[0].toUpperCase() + category.slice(1);
    });
    this.route.params.subscribe((params: Params) => {
      const id = params['id'];
      if(id) {
        this.http.get(`https://projectsattractor-default-rtdb.firebaseio.com//quotes/${id}.json`)
          .pipe(map(result => {
          if (result === null) {
            return [];
          }
          return Object(result);
        }))
          .subscribe(quote => {
            if (id) {
              this.category = quote.category;
              this.author = quote.author;
              this.text = quote.text;
            }
          });
        return this.id = id;
    }
    });
    }

  sendQuote(){
    const author = this.author;
    const category = this.category;
    const text = this.text;
    const body = {author, category, text};
    if(this.id){
      this.http.put(`https://projectsattractor-default-rtdb.firebaseio.com/quotes/${this.id}.json`, body).subscribe();
    } else {
      this.http.post('https://projectsattractor-default-rtdb.firebaseio.com/quotes.json', body).subscribe();
    }
    }

}
