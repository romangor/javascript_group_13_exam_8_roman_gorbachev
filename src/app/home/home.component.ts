import { Component, OnInit } from '@angular/core';
import { QuoteService } from '../shared/quote.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  categories!: string[];

  constructor(private quoteService: QuoteService) {}

  ngOnInit(): void {
    const categories = this.quoteService.getCategories();
    this.categories = categories.map(category => {
      if (!category) return category;
      return category[0].toUpperCase() + category.slice(1);
    });
  }

}
