import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Quote } from '../shared/quote.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.css']
})
export class QuotesComponent implements OnInit {
  quotes!: Quote[];
  noQuote = false;
  category!: string;
  url!: string;
  preloader = false;

  constructor(private route: ActivatedRoute,
              private http: HttpClient) { }

  ngOnInit(): void {
    this.preloader = true;
    this.route.params.subscribe( (params: Params) => {
      const category = params['id'];
        if(!category){
          this.url = 'https://projectsattractor-default-rtdb.firebaseio.com/quotes.json';
        } else {
          this.url =`https://projectsattractor-default-rtdb.firebaseio.com//quotes.json?orderBy="category"&equalTo="${category}"`
        }
          this.http.get<{ [ id:string ]: Quote}>(this.url)
            .pipe(map( result =>{
              return Object.keys(result).map(id =>{
                const quote = result[id];
                return new Quote (id, quote.author, quote.category, quote.text);
              })
          }))
            .subscribe(quotes =>{
              this.preloader = false;
              if(quotes.length === 0){
                this.noQuote = true;
              } else {
                this.noQuote = false;
                this.quotes = quotes;
              }
            })
        return this.category = category;
    })

  }

  onDelete(id: string, index: number) {
    this.http.delete(`https://projectsattractor-default-rtdb.firebaseio.com//quotes/${id}.json`).subscribe()
    this.quotes.splice(index, 1);
    return this.quotes;
  }
}
